# Goals
* install updates
* no user-interaction
* not breaking something by the updater itself

## Problems
1. Need the updates in cache, otherwise it would take much to long to shut down
2. Need some kind of notification in case the update failed
3. Arch keyring may run out of date for some user and updates might fail
4. Users might shut down the PC while the download runs. This would install a partial-state
5. packages might conflict with users normal pacman update packages

# How to reach the goals
1. I use a downloader while the session is up and a installerthat kicks in the update process on shutdown.
2. Not sure what we do about this right now
3. I plan to check for archlinux-keyring* and install this at first if found
4. I might set an additional lock file that, if found will not trigger the background installer on shutdown.
5. I also want to make sure that packages of the normal pacman process, dont do something crazy. So the updater downloads to "/var/cache/sd-pacman-update/pkg" instead of pacmans own cache.
