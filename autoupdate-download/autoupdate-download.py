#!/usr/bin/env python3
## Script to load pacman updates in the background
## also tries to keep them clean for installation

import configparser
import fnmatch
import io
import os
import pycurl
from pydbus import SystemBus
import subprocess
import sys

def funct_error_exit(error_message):
    print(error_message, file=sys.stderr)
    sys.exit(1)
def funct_normal_exit(normal_message):
    print(normal_message, file=sys.stdout)
    sys.exit(0)

# Set LANG to C, so that command output is more predictable
os.environ['LANG'] = "C"

# Streamline config naming
AUTOUPDATE_CONF_PATH = '/usr/share/arch-autoupdate/autoupdate.conf'

# Read config
if not os.path.isfile(AUTOUPDATE_CONF_PATH):
    funct_error_exit("Config file not found")
    
## allow_no_value=True <- to allow keys without value, like ILoveCandy
## strict=False <- do not run into errors, when duplicates are in the file
autoupdate_config = configparser.ConfigParser(allow_no_value=True, strict=False)
autoupdate_config.read(AUTOUPDATE_CONF_PATH)
## Set the vars we need in here
UPDATER_CACHE_PATH = autoupdate_config.get('options', 'CacheDir',
                                           fallback='/var/cache/autoupdate/pkg')
NETWORKCHECKURL = "https://www.archlinux.org"

## Check pathes for vars
## Cache parent at first :D
PARENT_UPDATER_CACHE_PATH = os.path.abspath(os.path.join(UPDATER_CACHE_PATH, os.pardir))
if not os.path.isdir(PARENT_UPDATER_CACHE_PATH):
    print(PARENT_UPDATER_CACHE_PATH, " not found. create it")
    os.mkdir(PARENT_UPDATER_CACHE_PATH, mode=0o700)

if not os.path.isdir(UPDATER_CACHE_PATH):
    print(UPDATER_CACHE_PATH, " not found. create it")
    os.mkdir(UPDATER_CACHE_PATH, mode=0o700)

# Metered connection
## The updater should not run, when the connection is metered.
## Networkmanager provides support for checking that.
## https://developer.gnome.org/NetworkManager/unstable/nm-dbus-types.html#enum%20NMMetered
sys_dbus_org_freedesktop_networkmanager = SystemBus().get("org.freedesktop.NetworkManager",
                                                          "/org/freedesktop/NetworkManager") 
if sys_dbus_org_freedesktop_networkmanager.Metered == 1:
    funct_normal_exit("Connection is metered, skip download")
elif sys_dbus_org_freedesktop_networkmanager.Metered == 2:
    print("Connection is not metered")
elif sys_dbus_org_freedesktop_networkmanager.Metered == 3:
    funct_normal_exit("Connection is probably metered, skip download")
elif sys_dbus_org_freedesktop_networkmanager.Metered == 4:
    print("Connection appears to be not metered")
else:
    funct_error_exit("Something went wront by determine the metered state of your connection.")

# Connect to the Internet to check if you can actually reach it
## curl up to the site, write it to a buffer (buff)
bffr = io.BytesIO()
crl = pycurl.Curl()
crl.setopt(crl.URL, NETWORKCHECKURL)
crl.setopt(crl.WRITEFUNCTION, bffr.write)
crl.perform()
HTTP_CODE = crl.getinfo(crl.RESPONSE_CODE)
crl.close()

## check the http code. We need a 200 and nothing else.
if HTTP_CODE != 200:
    print("HTTP CODE: %s" % HTTP_CODE, file=sys.stderr)
    funct_normal_exit("System is not able to connect to the Internet, Skip download")

# Check for repo files and remove them
REPO_DB_FILES = ["autoupdate.db", "autoupdate.db.tar.gz", "autoupdate.files",
                "autoupdate.files.tar.gz", "autoupdate.db.tar.gz.old",
                "autoupdate.files.tar.gz.old"]
REPO_DB_VALUE = 0
while REPO_DB_VALUE < len(REPO_DB_FILES):
    if os.path.isfile(os.path.join(UPDATER_CACHE_PATH, REPO_DB_FILES[REPO_DB_VALUE])):
        os.remove(os.path.join(UPDATER_CACHE_PATH, REPO_DB_FILES[REPO_DB_VALUE]))
    REPO_DB_VALUE = REPO_DB_VALUE + 1

# Call pacman
## Sync and download everything
args_app_pacman = ["/usr/bin/pacman", "--sync", "--cachedir",
                   UPDATER_CACHE_PATH, "--refresh", "--sysupgrade",
                   "--downloadonly","--noprogressbar", "--noconfirm",
                   "--verbose"]
app_pacman = subprocess.run(args_app_pacman,
                            capture_output=True,
                            universal_newlines=True)

if app_pacman.returncode != 0:
    funct_error_exit(app_pacman.stderr)

# Cleanup packages
## Because it will lead to conflicts if we have multiple versions of one package installed
## it's nesessary to clean up the packages before creating the repo files
if len(os.listdir(UPDATER_CACHE_PATH)) >= 2:
    args_app_paccache = ["/usr/bin/paccache", "--remove", "--verbose", 
                            "--keep", "1", "--cachedir", UPDATER_CACHE_PATH]
    app_paccache = subprocess.run(args_app_paccache,
                                  capture_output=True,
                                  universal_newlines=True)
    if app_paccache.returncode != 0:
        funct_error_exit(app_paccache.stderr)

# Call repo-add
## IF there are files loaded, create a repo to use for the installer job
if len(os.listdir(UPDATER_CACHE_PATH)) >= 1:
    args_app_repoadd = ["/usr/bin/repo-add", os.path.join(UPDATER_CACHE_PATH, "autoupdate.db.tar.gz")]
    for file in os.listdir(UPDATER_CACHE_PATH):
        if fnmatch.fnmatch(file, '*.pkg.*'):
            print("Update in cache: " + file, file=sys.stdout)
            args_app_repoadd.append(os.path.join(UPDATER_CACHE_PATH, file))

    app_repoadd = subprocess.run(args_app_repoadd,
                                 capture_output=True,
                                 universal_newlines=True)

    if app_repoadd.returncode != 0:
        funct_error_exit(app_repoadd.stderr)

sys.exit(0)
